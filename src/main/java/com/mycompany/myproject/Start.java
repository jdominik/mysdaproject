/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myproject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author RENT
 */
public class Start {

    private static String url = "jdbc:mysql://localhost/books_db?serverTimezone=CET";
    private static String userName = "root";
    private static String password = "";

    public static void main(String... args) throws ClassNotFoundException, SQLException {
        DBConnectionManager dbConnectionManager = new DBConnectionManager(url, userName, password);
        Connection connection = dbConnectionManager.getConnection();        // mamy obiekt połączenia
        java.sql.PreparedStatement ps = null;                               // połączenie ma metodę przygotuj zapytanie
        try {
            ps = connection.prepareStatement("select id, firstname, lastname from users");
            ResultSet resultSet = ps.executeQuery();            // on zawiera zbiór (więc kolejność nie ma znaczenia, ale i tak powinna być posortowana tak jak zwróciło ją zapytanie do bazy, a elementy sie nie powtarzaja!) krotek
            while (resultSet.next()) {                          // ResultSet jest obiektem, który opakowuje kursor w javie
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");

                System.out.println(firstName + " " + lastName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // java.sql.PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement("insert into users(firstname,lastname) values (?, ?)");
            ps.setString(1, "Humon");
            ps.setString(2, "Marcinkowski");
            ps.execute();       // zwraca prawda/fałsz, natomiast executeupdate zwraca ilość zmodyfikowanych rekordów
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
